﻿using BerryCluster.src.Features.SharedMemoryFeature;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace BerryCluster
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            //tests
            /*List<SharedMemoryNodeInfo> NodesInfo = new List<SharedMemoryNodeInfo>();

            NodesInfo.Add(new SharedMemoryNodeInfo(null, null, 1));
            NodesInfo.Add(new SharedMemoryNodeInfo(null, null, 2));

            SharedMemoryStream Stream = new SharedMemoryStream(NodesInfo.ToArray());
            Stream.Position = 900000;
            Stream.Write(new byte[2300000], 0, 2300000);*/


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}

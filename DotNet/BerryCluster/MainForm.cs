﻿using BerryCluster.src;
using BerryCluster.src.Network;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace BerryCluster
{
    public partial class MainForm : Form
    {
        public Server server;
        public static MainForm Form;

        private List<Peer> Nodes = new List<Peer>();

        public MainForm()
        {
            InitializeComponent();
            Form = this;
            this.server = new Server(8322);
            ThreadPool.QueueUserWorkItem(onUpdateNodes);
        }

        private void onUpdateNodes(object o)
        {
            while(!base.IsDisposed)
            {
                lock (Form.NodeLv)
                {
                    Form.Invoke(new Invoky(() =>
                    {
                        for (int i = 0; i < Form.NodeLv.Items.Count; i++)
                        {
                            Peer peer = Form.NodeLv.Items[i].Tag as Peer;
                            if (peer != null)
                            {
                                peer.ListItem.SubItems[4].Text = Math.Round(((double)(peer.NetReadSpeed / 1000F) / 1000F) * 8F, 2) + " Mbit";
                                peer.ListItem.SubItems[5].Text = Math.Round(((double)(peer.NetWriteSpeed / 1000F) / 1000F) * 8F, 2) + " Mbit";
                            }
                        }
                    }));
                }
                Thread.Sleep(1000);
            }
        }

        public static void onNewNode(Peer peer)
        {
            lock (Form.Nodes)
            {
                Form.Invoke(new Invoky(() =>
                {
                    peer.ListItem = new ListViewItem(new string[]
                    {
                        peer.RemoteIP,
                        peer.DeviceName,
                        peer.CpuCores.ToString(),
                        peer.MaxMemory_MB + "MB",
                        "0 Mbit",
                        "0 Mbit"
                    });
                    peer.ListItem.Tag = peer;

                    lock (Form.NodeLv)
                    {
                        Form.NodeLv.Items.Add(peer.ListItem);
                    }
                }));
                Form.Nodes.Add(peer);

                Form.Invoke(new Invoky(() =>
                {
                    int CoreCount = 0;
                    ulong MemoryCount = 0;

                    foreach (Peer p in Form.Nodes)
                    {
                        CoreCount += p.CpuCores;
                        MemoryCount += (ulong)p.MaxMemory_MB;
                    }

                    Form.Coreslbl.Text = "Total Cores: " + CoreCount;
                    Form.Memorylbl.Text = "Total Memory: " + MemoryCount + "MB";
                    Form.Clientslbl.Text = "Clients: " + Form.Nodes.Count;
                }));
            }
        }

        private void shareMemoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new SharedMemForm(GetSelectedNodes()).ShowDialog();
        }

        private Peer[] GetSelectedNodes()
        {
            lock(NodeLv)
            {
                List<Peer> nodes = new List<Peer>();
                for (int i = 0; i < NodeLv.SelectedItems.Count; i++)
                    nodes.Add(NodeLv.SelectedItems[i].Tag as Peer);
                return nodes.ToArray();
            }
        }

        private void brutefoceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new BruteforceForm(GetSelectedNodes()).Show();
        }

        private void hashCollisionFinderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new HashCollisionForm(GetSelectedNodes()).Show();
        }
    }
}

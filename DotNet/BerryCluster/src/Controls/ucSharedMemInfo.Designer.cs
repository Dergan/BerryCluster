﻿namespace BerryCluster.src.Controls
{
    partial class ucSharedMemInfo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Hostlbl = new System.Windows.Forms.Label();
            this.MaxMemlbl = new System.Windows.Forms.Label();
            this.FreeMemlbl = new System.Windows.Forms.Label();
            this.UseMemBar = new System.Windows.Forms.TrackBar();
            this.UseMemlbl = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.UseMemBar)).BeginInit();
            this.SuspendLayout();
            // 
            // Hostlbl
            // 
            this.Hostlbl.AutoSize = true;
            this.Hostlbl.Location = new System.Drawing.Point(33, 12);
            this.Hostlbl.Name = "Hostlbl";
            this.Hostlbl.Size = new System.Drawing.Size(35, 13);
            this.Hostlbl.TabIndex = 0;
            this.Hostlbl.Text = "Host: ";
            // 
            // MaxMemlbl
            // 
            this.MaxMemlbl.AutoSize = true;
            this.MaxMemlbl.Location = new System.Drawing.Point(192, 12);
            this.MaxMemlbl.Name = "MaxMemlbl";
            this.MaxMemlbl.Size = new System.Drawing.Size(73, 13);
            this.MaxMemlbl.TabIndex = 1;
            this.MaxMemlbl.Text = "Max Memory: ";
            // 
            // FreeMemlbl
            // 
            this.FreeMemlbl.AutoSize = true;
            this.FreeMemlbl.Location = new System.Drawing.Point(330, 12);
            this.FreeMemlbl.Name = "FreeMemlbl";
            this.FreeMemlbl.Size = new System.Drawing.Size(74, 13);
            this.FreeMemlbl.TabIndex = 2;
            this.FreeMemlbl.Text = "Free Memory: ";
            // 
            // UseMemBar
            // 
            this.UseMemBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UseMemBar.Location = new System.Drawing.Point(36, 35);
            this.UseMemBar.Name = "UseMemBar";
            this.UseMemBar.Size = new System.Drawing.Size(637, 45);
            this.UseMemBar.TabIndex = 3;
            this.UseMemBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.UseMemBar.Scroll += new System.EventHandler(this.UseMemBar_Scroll);
            // 
            // UseMemlbl
            // 
            this.UseMemlbl.AutoSize = true;
            this.UseMemlbl.Location = new System.Drawing.Point(482, 12);
            this.UseMemlbl.Name = "UseMemlbl";
            this.UseMemlbl.Size = new System.Drawing.Size(72, 13);
            this.UseMemlbl.TabIndex = 4;
            this.UseMemlbl.Text = "Use Memory: ";
            // 
            // ucSharedMemInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.UseMemlbl);
            this.Controls.Add(this.UseMemBar);
            this.Controls.Add(this.FreeMemlbl);
            this.Controls.Add(this.MaxMemlbl);
            this.Controls.Add(this.Hostlbl);
            this.Name = "ucSharedMemInfo";
            this.Size = new System.Drawing.Size(688, 83);
            ((System.ComponentModel.ISupportInitialize)(this.UseMemBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label Hostlbl;
        public System.Windows.Forms.Label MaxMemlbl;
        public System.Windows.Forms.Label FreeMemlbl;
        public System.Windows.Forms.TrackBar UseMemBar;
        public System.Windows.Forms.Label UseMemlbl;

    }
}

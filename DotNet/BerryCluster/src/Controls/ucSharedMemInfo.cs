﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using BerryCluster.src.Network;

namespace BerryCluster.src.Controls
{
    public partial class ucSharedMemInfo : UserControl
    {
        public Peer Node { get; private set; }

        public ucSharedMemInfo(Peer node)
        {
            InitializeComponent();
            this.Node = node;
            Hostlbl.Text += node.RemoteIP;
            MaxMemlbl.Text += node.MaxMemory_MB + " MB";
            FreeMemlbl.Text += node.FreeMemory_MB + " MB";

            UseMemBar.Minimum = 1;
            UseMemBar.Maximum = node.FreeMemory_MB;

            if ((double)node.FreeMemory_MB * 0.80F > UseMemBar.Minimum)
                UseMemBar.Value = (int)((double)node.FreeMemory_MB * 0.80F);
            UseMemlbl.Text = "Use Memory: " + UseMemBar.Value + " MB";
        }

        private void UseMemBar_Scroll(object sender, EventArgs e)
        {
            UseMemlbl.Text = "Use Memory: " + UseMemBar.Value + " MB";
        }

    }
}
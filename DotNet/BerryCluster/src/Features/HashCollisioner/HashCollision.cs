﻿using BerryCluster.src.Network;
using System;
using System.Collections.Generic;
using System.Text;

namespace BerryCluster.src.Features.HashCollisioner
{
    public class HashCollision : IFeature
    {
        //Messages
        public const byte BRUTEFORCE = 1;
        public const byte BRUTEFORCE_RESPONSE = 2;
        public const byte BRUTEFORCE_SPEED = 3;
        public const byte BRUTEFORCE_STOP = 4;

        //Types
        public const byte TYPE_MD5 = 1;
        public const byte TYPE_SHA1 = 2;
        public const byte TYPE_SHA256 = 3;
        public const byte TYPE_SHA512 = 4;
        public const byte TYPE_CRC32 = 5;

        public int SpeedPerSec { get; private set; }
        public long HashesCompleted { get; private set; }
        public long CurrentSize { get; private set; }

        public bool SuccessCollision { get; private set; }
        public byte[] CollisionData { get; private set; }

        public HashCollision(Peer peer)
            : base(peer)
        {

        }

        public override int FeatureId
        {
            get { return 3; }
        }

        public override void onReceiveMessage(byte[] data)
        {
            if(data.Length > 0)
            {
                PayloadReader pr = new PayloadReader(data);
                switch(pr.ReadByte())
                {
                    case BRUTEFORCE_SPEED:
                    {
                        SpeedPerSec = pr.ReadInteger();
                        CurrentSize = pr.ReadInteger();

                        byte[] tempData = pr.ReadBytes(8);
                        Array.Reverse(tempData);
                        HashesCompleted = BitConverter.ToInt64(tempData, 0);
                        break;
                    }
                    case BRUTEFORCE_RESPONSE:
                    {
                        SuccessCollision = pr.ReadByte() == 1;
                        CollisionData = pr.ReadBytes(pr.ReadInteger());
                        break;
                    }
                }
            }
        }

        public void FindCollision(byte[] Hash, byte[] Data, byte UseAlgo, int StartSize, int HopTime)
        {
            SuccessCollision = false;

            using (PayloadWriter pw = new PayloadWriter())
            {
                pw.WriteByte(BRUTEFORCE);

                pw.WriteInteger(Hash.Length);
                pw.WriteBytes(Hash);

                pw.WriteInteger(Data.Length);
                pw.WriteBytes(Data);

                if (UseAlgo == TYPE_CRC32)
                    pw.WriteLong_Java(BitConverter.ToUInt32(Hash, 0));
                else
                    pw.WriteLong_Java(0);

                pw.WriteByte(UseAlgo);

                pw.WriteInteger(StartSize);
                pw.WriteInteger(HopTime);

                base.SendMessage(pw);
            }
        }

        public void StopBruteforce()
        {
            using (PayloadWriter pw = new PayloadWriter())
            {
                pw.WriteByte(BRUTEFORCE_STOP);
                base.SendMessage(pw);
            }
        }
    }
}
﻿using BerryCluster.src.Network;
using BerryCluster.src.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace BerryCluster.src.Features.SharedMemoryFeature
{
    public class SharedMemory : IFeature
    {
        private const byte RESET_MEMORY = 1;
        private const byte ALLOCATE_MEMORY = 2;
        private const byte WRITE_MEMORY = 3;
        private const byte READ_MEMORY = 4;
	    private const byte READ_MEMORY_RESPONSE = 5;

        private SyncObject ReadDataSync;
        private object ReadLock = new object();

        public SharedMemory(Peer peer)
            : base(peer)
        {

        }

        public override int FeatureId
        {
            get { return 1; }
        }

        public override void onReceiveMessage(byte[] data)
        {
            if (data.Length == 0)
                return;

            switch(data[0])
            {
                case READ_MEMORY_RESPONSE:
                {
                    byte[] Temp = new byte[data.Length - 1];
                    Array.Copy(data, 1, Temp, 0, Temp.Length);
                    ReadDataSync.Value = Temp;
                    ReadDataSync.Pulse();
                    break;
                }
            }
        }

        public void SendResetMemory()
        {
            base.SendMessage(new byte[] { RESET_MEMORY });
        }

        public void SendAllocateMemory(int AllocationSize)
        {
            using (PayloadWriter pw = new PayloadWriter())
            {
                pw.WriteByte(ALLOCATE_MEMORY);
                pw.WriteInteger(AllocationSize);
                base.SendMessage(pw);
            }
        }

        public void SendWriteMemory(byte[] Data, int Position)
        {
            using (PayloadWriter pw = new PayloadWriter())
            {
                pw.WriteByte(WRITE_MEMORY);
                pw.WriteInteger(Position);
                pw.WriteInteger(Data.Length);
                pw.WriteBytes(Data);
                base.SendMessage(pw);
            }
        }

        public byte[] ReadMemory(int Position, int Length)
        {
            lock (ReadLock)
            {
                using (PayloadWriter pw = new PayloadWriter())
                {
                    ReadDataSync = new SyncObject(base.Peer);

                    pw.WriteByte(READ_MEMORY);
                    pw.WriteInteger(Position);
                    pw.WriteInteger(Length);
                    base.SendMessage(pw);

                    return ReadDataSync.Wait<byte[]>(null, 30000);
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace BerryCluster.src.Features.SharedMemoryFeature
{
    public class SharedMemoryStream : Stream
    {
        private long Pos = 0;

        public SharedMemoryNodeInfo[] Nodes { get; private set; }


        public SharedMemoryStream(SharedMemoryNodeInfo[] Nodes)
        {
            this.Nodes = Nodes;

            //reset all memory in the Raspberries
            foreach (SharedMemoryNodeInfo node in Nodes)
                node.SharedMem.SendResetMemory();

            //allocate memory in the Raspberries
            foreach (SharedMemoryNodeInfo node in Nodes)
            {
                node.SharedMem.SendAllocateMemory(node.UseMemory);
            }
        }

        public override bool CanRead
        {
            get { return true; }
        }

        public override bool CanSeek
        {
            get { return true; }
        }

        public override bool CanWrite
        {
            get { return true; }
        }

        public override void Flush()
        {

        }

        public override long Length
        {
            get
            {
                long temp = 0;
                for (int i = 0; i < Nodes.Length; i++)
                    temp += Nodes[i].UseMemory;
                return temp;
            }
        }

        public override long Position
        {
            get
            {
                return Pos;
            }
            set
            {
                Pos = value;
            }
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            int TotalRead = 0;

            foreach(NodeOffsetInfo off in GetRWOffsets(this.Position, count))
            {
                byte[] TempData = off.Node.SharedMem.ReadMemory(off.Position, off.Length);

                if (TempData == null)
                    throw new TimeoutException();

                Array.Copy(TempData, 0, buffer, offset, TempData.Length);

                offset += off.Length;
                Position += off.Length;
                TotalRead += off.Length;
            }
            return TotalRead;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {

            return -1;
        }

        public override void SetLength(long value)
        {
            throw new NotSupportedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            foreach (NodeOffsetInfo off in GetRWOffsets(Position, count))
            {
                byte[] TempData = new byte[off.Length];
                Array.Copy(buffer, offset, TempData, 0, TempData.Length);

                off.Node.SharedMem.SendWriteMemory(TempData, off.Position);

                count -= off.Length;
                offset += off.Length;
                Position += off.Length;
            }
        }

        private NodeOffsetInfo[] GetRWOffsets(long Position, int Length)
        {
            List<NodeOffsetInfo> Offets = new List<NodeOffsetInfo>();

            long PrevBlockSizes = 0;

            foreach(SharedMemoryNodeInfo node in this.Nodes)
            {
                while (Position < (PrevBlockSizes + node.UseMemory) && Length > 0)
                {
                    int size = Length > short.MaxValue ? short.MaxValue : Length;
                    int offset = (int)(Position - PrevBlockSizes);

                    if (offset + size > node.UseMemory)
                    {
                        size = node.UseMemory - offset;
                    }

                    Offets.Add(new NodeOffsetInfo(node, offset, size));

                    Length -= size;
                    Position += size;
                }
                PrevBlockSizes += node.UseMemory;

                if (Length == 0)
                    break;
            }

            if (Length > 0)
                throw new OutOfMemoryException("Not enough memory allocated or Position/Offset is out of bounds");

            return Offets.ToArray();
        }

        public class NodeOffsetInfo
        {
            public SharedMemoryNodeInfo Node { get; private set; }
            public int Position { get; private set; }
            public int Length { get; private set; }

            public NodeOffsetInfo(SharedMemoryNodeInfo Node, int Position, int Length)
            {
                this.Node = Node;
                this.Position = Position;
                this.Length = Length;
            }

            public override string ToString()
            {
                return "Position=" + Position + ", Length=" + Length + ", Node=" + (Node.Peer != null ? Node.Peer.DeviceName : Node.UseMemory.ToString());
            }
        }
    }
}
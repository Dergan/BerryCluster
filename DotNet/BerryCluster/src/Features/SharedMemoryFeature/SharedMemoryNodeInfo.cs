﻿using BerryCluster.src.Features.SharedMemoryFeature;
using BerryCluster.src.Network;
using System;
using System.Collections.Generic;
using System.Text;

namespace BerryCluster.src.Features.SharedMemoryFeature
{
    public class SharedMemoryNodeInfo
    {
        public SharedMemory SharedMem { get; private set; }
        public Peer Peer { get; private set; }
        public int UseMemory { get; private set; }

        public SharedMemoryNodeInfo(SharedMemory SharedMem, Peer Peer, int UseMemory)
        {
            this.SharedMem = SharedMem;
            this.Peer = Peer;
            this.UseMemory = UseMemory * 1000000;
        }
    }
}
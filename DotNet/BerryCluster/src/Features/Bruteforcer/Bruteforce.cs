﻿using BerryCluster.src.Network;
using BerryCluster.src.Utils;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace BerryCluster.src.Features.Bruteforcer
{
    public class Bruteforce : IFeature
    {
        //Messages
        public const byte SET_HASH = 1;
        public const byte BRUTEFORCE = 2;
        public const byte BRUTEFORCE_RESPONSE = 3;

        //Types
        public const byte TYPE_MD5 = 1;
        public const byte TYPE_SHA1 = 2;
        public const byte TYPE_SHA256 = 3;
        public const byte TYPE_SHA512 = 4;

        public ListViewItem ListItem { get; set; }
        private SyncObject BruteSync;
        private object SyncLock = new object();

        public string FilePath { get; set; }
        public long BeginPosition { get; set; }
        public long BruteLines { get; set; }

        public bool ThreadIsRunning { get; private set; }
        private Thread BruteThread;

        public object ThreadSafe = new object();
        public string CurrentPassword { get; private set; }
        public int Tries { get; set; }
        public int TotalTries { get; private set; }

        public string CrackedPassword { get; private set; }
        public bool IsCracked { get; private set; }

        public Bruteforce(Peer peer)
            : base(peer)
        {

        }

        public override int FeatureId
        {
            get { return 2; }
        }

        public override void onReceiveMessage(byte[] data)
        {
            if(data.Length > 0)
            {
                using(PayloadReader pr = new PayloadReader(data))
                {
                    switch(pr.ReadByte())
                    {
                        case BRUTEFORCE_RESPONSE:
                        {
                            BruteSync.Value = new BruteResult()
                            {
                                Success = pr.ReadByte() == 1,
                                Password = pr.ReadBytes(pr.ReadInteger())
                            };
                            BruteSync.Pulse();
                            break;
                        }
                    }
                }
            }
        }

        public bool SetHash(string Hash, byte UseAlgo)
        {
            //reset some info
            IsCracked = false;
            CrackedPassword = "";
            CurrentPassword = "";

            using (PayloadWriter pw = new PayloadWriter())
            {
                pw.WriteByte(SET_HASH);

                byte[] temp = ConvertHexStringToByteArray(Hash);

                if(temp == null)
                {
                    return false;
                }

                pw.WriteInteger(temp.Length);
                pw.WriteBytes(temp);

                pw.WriteByte(UseAlgo);
                base.SendMessage(pw);
                return true;
            }
        }

        /// <summary>
        /// Try a few passwords to bruteforce
        /// </summary>
        /// <param name="Passwords">The passwords to try</param>
        /// <param name="Password">The password that has been successfully bruteforced</param>
        /// <returns>If password has been found</returns>
        public bool TryPasswords(string[] Passwords, ref string Password)
        {
            lock(SyncLock)
            {
                BruteSync = new SyncObject(base.Peer);
                using(PayloadWriter pw = new PayloadWriter())
                {
                    pw.WriteByte(BRUTEFORCE);
                    pw.WriteInteger(Passwords.Length);

                    for (int i = 0; i < Passwords.Length; i++)
                    {
                        byte[] temp = ASCIIEncoding.UTF8.GetBytes(Passwords[i]);
                        pw.WriteInteger(temp.Length);
                        pw.WriteBytes(temp);
                    }
                    base.SendMessage(pw);
                }

                BruteResult result = BruteSync.Wait <BruteResult>(null, 30000);

                if(result == null)
                {
                    Password = "";
                    return false;
                }

                Password = ASCIIEncoding.UTF8.GetString(result.Password);
                return result.Success;
            }
        }

        //http://stackoverflow.com/a/8235530
        public static byte[] ConvertHexStringToByteArray(string hexString)
        {
            if (hexString.Length % 2 != 0)
            {
                return null;
            }

            byte[] HexAsBytes = new byte[hexString.Length / 2];
            for (int index = 0; index < HexAsBytes.Length; index++)
            {
                string byteValue = hexString.Substring(index * 2, 2);
                HexAsBytes[index] = byte.Parse(byteValue, NumberStyles.HexNumber, CultureInfo.InvariantCulture);
            }

            return HexAsBytes;
        }

        public void StartThread()
        {
            BruteThread = new Thread(new ThreadStart(onBruteThread));
            BruteThread.Start();
        }

        public void StopThread()
        {
            ThreadIsRunning = false;
            try
            {
                BruteThread.Abort();
            }
            catch { }
        }

        private void onBruteThread()
        {
            ThreadIsRunning = true;

            using (StreamReader sr = new StreamReader(this.FilePath))
            {
                sr.BaseStream.Position = this.BeginPosition;

                List<string> Dictionary = new List<string>();
                int curBruteLine = 0;

                while (ThreadIsRunning)
                {
                    Dictionary.Clear();
                    
                    long BeginPos = sr.BaseStream.Position;

                    while (sr.BaseStream.Position - BeginPos < 1000000 && curBruteLine < BruteLines) //1MB
                    {
                        string read = sr.ReadLine();
                        if (read == null)
                            break;
                        Dictionary.Add(read);
                        curBruteLine++;
                    }

                    if(Dictionary.Count > 0)
                    {
                        this.CurrentPassword = Dictionary[0];

                        string Pass = "";
                        if(TryPasswords(Dictionary.ToArray(), ref Pass))
                        {
                            //cracked
                            this.CrackedPassword = Pass;
                            this.IsCracked = true;
                            break;
                        }

                        lock (ThreadSafe)
                        {
                            this.Tries += Dictionary.Count;
                        }
                        
                        this.TotalTries += Dictionary.Count;
                    }

                    if (curBruteLine + 1 == BruteLines)
                        break;
                }
            }
            ThreadIsRunning = false;
        }

        private class BruteResult
        {
            public bool Success = false;
            public byte[] Password = new byte[0];
        }
    }
}
﻿using BerryCluster.src.Network;
using System;
using System.Collections.Generic;
using System.Text;

namespace BerryCluster.src.Features
{
    public abstract class IFeature
    {
        public Peer Peer { get; private set; }
        public Peer[] Nodes { get; private set; }

        public abstract int FeatureId { get; }
        public abstract void onReceiveMessage(byte[] data);

        public IFeature(Peer Peer)
        {
            this.Peer = Peer;
            this.Nodes = new Peer[0];
        }

        public void SendMessage(PayloadWriter pw)
        {
            SendMessage(pw.ToByteArray());
        }

        public void SendMessage(byte[] data)
        {
            using(PayloadWriter pw = new PayloadWriter())
            {
                pw.WriteByte((byte)Peer.Command.DATA);
                pw.WriteInteger(FeatureId);
                pw.WriteBytes(data);
                Peer.SendMessage(pw);
            }
        }
    }
}
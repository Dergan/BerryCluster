﻿using BerryCluster.src.Features;
using BerryCluster.src.Features.Bruteforcer;
using BerryCluster.src.Features.HashCollisioner;
using BerryCluster.src.Features.SharedMemoryFeature;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;

namespace BerryCluster.src.Network
{
    public class Peer
    {
        public enum ReceiveType { Header, Payload }

        public enum Command
        {
            PONG = 0,
            PING = 1,
            CONNECT = 2,
            DATA = 3,
        }

        private const int HEADER_SIZE = 4;
        public Socket Handle { get; private set; }
        public bool Connected { get; private set; }

        private int ReadableDataLen = 0;
        private int PayloadLen = 0;
        private int ReadOffset = 0;
        private int TotalReceived = 0;
        private int WriteOffset = 0;

        private ReceiveType ReceiveState = ReceiveType.Header;

        private byte[] Buffer;

        public ListViewItem ListItem { get; set; }

        public ulong TotalSizeCompleted { get; private set; }
        public ulong SpeedPerSec { get; private set; }
        private Stopwatch SpeedSW = Stopwatch.StartNew();

        //node info
        public string RemoteIP { get; private set; }
        public string DeviceName { get; private set; }
        public int CpuCores { get; private set; }
        public int MaxMemory_MB { get; private set; }
        public int FreeMemory_MB { get; private set; }

        private List<IFeature> Features;

        public SharedMemory SharedMemory { get; private set; }
        public Bruteforce Bruteforce { get; private set; }
        public HashCollision HashCollision { get; private set; }

        private Stopwatch NetReadSpeedSW = Stopwatch.StartNew();
        private long _NetReadSpeed = 0;
        public long NetReadSpeed { get; private set; }

        private Stopwatch NetWriteSpeedSW = Stopwatch.StartNew();
        private long _NetWriteSpeed = 0;
        public long NetWriteSpeed { get; private set; }

        public Peer(Socket socket)
        {
            this.Handle = socket;
            this.Connected = true;
            RemoteIP = Handle.RemoteEndPoint.ToString().Split(':')[0];

            this.SharedMemory = new src.Features.SharedMemoryFeature.SharedMemory(this);
            this.Bruteforce = new src.Features.Bruteforcer.Bruteforce(this);
            this.HashCollision = new src.Features.HashCollisioner.HashCollision(this);

            Features = new List<IFeature>();
            Features.Add(this.SharedMemory);
            Features.Add(this.Bruteforce);
            Features.Add(this.HashCollision);

            this.Buffer = new byte[70000];
            Handle.BeginReceive(Buffer, 0, Buffer.Length, SocketFlags.None, AynsReceive, null);
        }

        private void AynsReceive(IAsyncResult result)
        {
            int BytesTransferred = -1;
            try
            {
                BytesTransferred = Handle.EndReceive(result);
                if (BytesTransferred <= 0)
                {
                    this.Connected = false;
                    return;
                }
            }
            catch
            {
                this.Connected = false;
                return;
            }

            //register speed
            _NetReadSpeed += BytesTransferred;
            if (NetReadSpeedSW.ElapsedMilliseconds >= 1000)
            {
                NetReadSpeed = _NetReadSpeed;
                _NetReadSpeed = 0;
                NetReadSpeedSW = Stopwatch.StartNew();
            }

            ReadableDataLen += BytesTransferred;
            bool Process = true;

            while (Process)
            {
                if (ReceiveState == ReceiveType.Header)
                {
                    Process = ReadableDataLen >= HEADER_SIZE;
                    if (ReadableDataLen >= HEADER_SIZE)
                    {
                        //Array.Reverse(Buffer, ReadOffset, 4);
                        PayloadLen = (int)BitConverter.ToUInt32(Buffer, ReadOffset);

                        TotalReceived = HEADER_SIZE;
                        ReadableDataLen -= HEADER_SIZE;
                        ReadOffset += HEADER_SIZE;
                        ReceiveState = ReceiveType.Payload;
                    }
                }
                else if (ReceiveState == ReceiveType.Payload)
                {
                    Process = ReadableDataLen >= PayloadLen;
                    if (ReadableDataLen >= PayloadLen)
                    {
                        using (PayloadReader pr = new PayloadReader(this.Buffer))
                        {
                            pr.Offset = ReadOffset;

                            if (PayloadLen > 0)
                            {
                                Command cmd = (Command)pr.ReadByte();

                                switch (cmd)
                                {
                                    case Command.PING:
                                    {
                                        using (PayloadWriter pw = new PayloadWriter())
                                        {
                                            pw.WriteBytes(new byte[] { (byte)Command.PONG, 6 });
                                            SendMessage(pw);
                                        }
                                        break;
                                    }
                                    case Command.CONNECT:
                                    {
                                        this.DeviceName = pr.ReadString();
                                        this.CpuCores = pr.ReadInteger();
                                        this.MaxMemory_MB = pr.ReadInteger();
                                        this.FreeMemory_MB = pr.ReadInteger();
                                        MainForm.onNewNode(this);
                                        break;
                                    }
                                    case Command.DATA:
                                    {
                                        int FeatureId = pr.ReadInteger();

                                        foreach(IFeature feature in Features)
                                        {
                                            if(feature.FeatureId == FeatureId)
                                            {
                                                byte[] tempData = pr.ReadBytes(PayloadLen - 5);
                                                feature.onReceiveMessage(tempData);
                                                break;
                                            }
                                        }
                                        break;
                                    }
                                    /*case Command.END_TASK:
                                    {
                                        TasksCompleted++;
                                        int OrgSize = pr.ReadInteger();
                                        int CompressedSize = pr.ReadInteger();
                                        TotalSizeCompleted += (ulong)OrgSize;

                                        SpeedPerSec += (ulong)OrgSize;

                                        if (SpeedSW.ElapsedMilliseconds >= 1000)
                                        {
                                            MainForm.onTaskCompleted(this);
                                            SpeedSW = Stopwatch.StartNew();
                                            SpeedPerSec = 0;
                                        }
                                        break;
                                    }*/
                                }
                            }
                        }

                        TotalReceived = 0;
                        ReadOffset += PayloadLen;
                        ReadableDataLen -= PayloadLen;
                        ReceiveState = ReceiveType.Header;
                    }
                }
            }

            int len = ReceiveState == ReceiveType.Header ? HEADER_SIZE : PayloadLen;
            if (ReadOffset + len >= this.Buffer.Length)
            {
                //no more room for this data size, at the end of the buffer ?

                //copy the buffer to the beginning
                Array.Copy(this.Buffer, ReadOffset, this.Buffer, 0, ReadableDataLen);

                WriteOffset = ReadableDataLen;
                ReadOffset = 0;
            }
            else
            {
                //payload fits in the buffer from the current offset
                //use BytesTransferred to write at the end of the payload
                //so that the data is not split
                WriteOffset += BytesTransferred;
            }

            try
            {
                Handle.BeginReceive(this.Buffer, WriteOffset, Buffer.Length - WriteOffset, SocketFlags.None, AynsReceive, null);
            }
            catch { }
        }

        public void SendMessage(PayloadWriter pw)
        {
            try
            {
                List<byte> temp = new List<byte>();
                temp.AddRange(BitConverter.GetBytes(pw.Length));
                temp.AddRange(pw.ToByteArray());
                Handle.Send(temp.ToArray(), 0, temp.Count, SocketFlags.None);

                //register speed
                _NetWriteSpeed += temp.Count;
                if (NetWriteSpeedSW.ElapsedMilliseconds >= 1000)
                {
                    NetWriteSpeed = _NetWriteSpeed;
                    _NetWriteSpeed = 0;
                    NetWriteSpeedSW = Stopwatch.StartNew();
                }
            }
            catch { }
        }
    }
}
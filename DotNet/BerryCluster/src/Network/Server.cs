﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace BerryCluster.src.Network
{
    public class Server
    {
        private Socket Handle;

        public List<Peer> Peers { get; private set; }

        public Server(int Port)
        {
            this.Peers = new List<Peer>();
            this.Handle = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            this.Handle.Bind(new IPEndPoint(0, Port));
            this.Handle.Listen(100);
            this.Handle.BeginAccept(AcceptCallback, null);
        }

        private void AcceptCallback(IAsyncResult ar)
        {
            try
            {
                this.Peers.Add(new Peer(Handle.EndAccept(ar)));
            }
            catch { }

            this.Handle.BeginAccept(AcceptCallback, null);
        }
    }
}
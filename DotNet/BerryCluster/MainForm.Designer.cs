﻿namespace BerryCluster
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.NodeLv = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.shareMemoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.brutefoceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hashCollisionFinderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Memorylbl = new System.Windows.Forms.Label();
            this.Coreslbl = new System.Windows.Forms.Label();
            this.Clientslbl = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(235, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Raspberry PI Cluster Server running at port 8322";
            // 
            // NodeLv
            // 
            this.NodeLv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NodeLv.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader7,
            this.columnHeader4,
            this.columnHeader5});
            this.NodeLv.ContextMenuStrip = this.contextMenuStrip1;
            this.NodeLv.FullRowSelect = true;
            this.NodeLv.GridLines = true;
            this.NodeLv.Location = new System.Drawing.Point(24, 102);
            this.NodeLv.Name = "NodeLv";
            this.NodeLv.Size = new System.Drawing.Size(488, 230);
            this.NodeLv.TabIndex = 1;
            this.NodeLv.UseCompatibleStateImageBehavior = false;
            this.NodeLv.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "IP Address";
            this.columnHeader1.Width = 99;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Hostname";
            this.columnHeader2.Width = 105;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Cores";
            this.columnHeader3.Width = 66;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Memory";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Read Mbit";
            this.columnHeader4.Width = 78;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Write Mbit";
            this.columnHeader5.Width = 71;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.shareMemoryToolStripMenuItem,
            this.brutefoceToolStripMenuItem,
            this.hashCollisionFinderToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(187, 70);
            // 
            // shareMemoryToolStripMenuItem
            // 
            this.shareMemoryToolStripMenuItem.Name = "shareMemoryToolStripMenuItem";
            this.shareMemoryToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.shareMemoryToolStripMenuItem.Text = "Share Memory";
            this.shareMemoryToolStripMenuItem.Click += new System.EventHandler(this.shareMemoryToolStripMenuItem_Click);
            // 
            // brutefoceToolStripMenuItem
            // 
            this.brutefoceToolStripMenuItem.Name = "brutefoceToolStripMenuItem";
            this.brutefoceToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.brutefoceToolStripMenuItem.Text = "Brutefoce";
            this.brutefoceToolStripMenuItem.Click += new System.EventHandler(this.brutefoceToolStripMenuItem_Click);
            // 
            // hashCollisionFinderToolStripMenuItem
            // 
            this.hashCollisionFinderToolStripMenuItem.Name = "hashCollisionFinderToolStripMenuItem";
            this.hashCollisionFinderToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.hashCollisionFinderToolStripMenuItem.Text = "Hash Collision Finder";
            this.hashCollisionFinderToolStripMenuItem.Click += new System.EventHandler(this.hashCollisionFinderToolStripMenuItem_Click);
            // 
            // Memorylbl
            // 
            this.Memorylbl.AutoSize = true;
            this.Memorylbl.Location = new System.Drawing.Point(30, 56);
            this.Memorylbl.Name = "Memorylbl";
            this.Memorylbl.Size = new System.Drawing.Size(99, 13);
            this.Memorylbl.TabIndex = 9;
            this.Memorylbl.Text = "Total Memory: 0MB";
            // 
            // Coreslbl
            // 
            this.Coreslbl.AutoSize = true;
            this.Coreslbl.Location = new System.Drawing.Point(30, 38);
            this.Coreslbl.Name = "Coreslbl";
            this.Coreslbl.Size = new System.Drawing.Size(73, 13);
            this.Coreslbl.TabIndex = 10;
            this.Coreslbl.Text = "Total Cores: 0";
            // 
            // Clientslbl
            // 
            this.Clientslbl.AutoSize = true;
            this.Clientslbl.Location = new System.Drawing.Point(30, 73);
            this.Clientslbl.Name = "Clientslbl";
            this.Clientslbl.Size = new System.Drawing.Size(50, 13);
            this.Clientslbl.TabIndex = 8;
            this.Clientslbl.Text = "Clients: 0";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(319, 8);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(94, 23);
            this.button2.TabIndex = 14;
            this.button2.Text = "Deployment";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(524, 357);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.Memorylbl);
            this.Controls.Add(this.Coreslbl);
            this.Controls.Add(this.Clientslbl);
            this.Controls.Add(this.NodeLv);
            this.Controls.Add(this.label1);
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.Text = "BerryCluster - Master Server";
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView NodeLv;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Label Memorylbl;
        private System.Windows.Forms.Label Coreslbl;
        private System.Windows.Forms.Label Clientslbl;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem shareMemoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem brutefoceToolStripMenuItem;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ToolStripMenuItem hashCollisionFinderToolStripMenuItem;
    }
}


﻿using BerryCluster.src.Controls;
using BerryCluster.src.Features;
using BerryCluster.src.Features.SharedMemoryFeature;
using BerryCluster.src.Network;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace BerryCluster
{
    public partial class SharedMemForm : Form
    {
        private List<SharedMemoryNodeInfo> NodesInfo = new List<SharedMemoryNodeInfo>();
        public SharedMemoryStream Stream { get; private set; }

        public SharedMemForm(Peer[] nodes)
        {
            InitializeComponent();

            foreach(Peer p in nodes)
            {
                flowLayoutPanel1.Controls.Add(new ucSharedMemInfo(p)
                {
                    //Dock = DockStyle.Fill
                });
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            InitNodes();

            Stream = new SharedMemoryStream(NodesInfo.ToArray());

            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                dialog.Multiselect = false;

                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    //as test let's load a image into memory, using multiple devices
                    byte[] ImageData = File.ReadAllBytes(dialog.FileName);
                    Stream.Write(ImageData, 0, ImageData.Length);

                    /*Stopwatch sw = Stopwatch.StartNew();
                    int Speed = 0;

                    while(true)
                    {
                        Stream.Write(ImageData, 0, ImageData.Length); //write to multiple devices
                        Stream.Position = 0;
                        Speed++;

                        if(sw.ElapsedMilliseconds >= 1000)
                        {
                            Debug.WriteLine("Speed:" + Speed);
                            sw = Stopwatch.StartNew();
                            Speed = 0;
                        }
                        Application.DoEvents();
                    }*/

                    //receive all data
                    Stream.Position = 0;
                    byte[] temp = new byte[ImageData.Length];
                    int read = Stream.Read(temp, 0, temp.Length);

                    Stream.Position = 0;
                    Bitmap bmp = (Bitmap)Bitmap.FromStream(Stream);
                    int width = bmp.Width;
                    int he = bmp.Height;
                    new ImageForm(bmp).Show();




                    //byte[] GrabbedImage = new byte[ImageData.Length];
                    //Stream.Read(GrabbedImage, 0, GrabbedImage.Length);
                    //new ImageForm((Bitmap)Bitmap.FromStream(new MemoryStream(GrabbedImage))).Show();
                }
            }
        }

        private void InitNodes()
        {
            NodesInfo.Clear();

            foreach (Control c in flowLayoutPanel1.Controls)
            {
                ucSharedMemInfo userControl = c as ucSharedMemInfo;
                NodesInfo.Add(new SharedMemoryNodeInfo(userControl.Node.SharedMemory, userControl.Node, userControl.UseMemBar.Value));
            }
        }
    }
}

﻿using BerryCluster.src;
using BerryCluster.src.Features.Bruteforcer;
using BerryCluster.src.Network;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace BerryCluster
{
    public partial class BruteforceForm : Form
    {
        public Peer[] Nodes { get; private set; }
        private Stopwatch RunTimeSW = null;

        public BruteforceForm(Peer[] nodes)
        {
            InitializeComponent();
            this.Nodes = nodes;

            foreach(Peer peer in nodes)
            {
                peer.Bruteforce.ListItem = new ListViewItem(new string[]
                {
                    peer.RemoteIP,
                    peer.DeviceName,
                    "",
                    "0",
                    ""
                });
                peer.Bruteforce.ListItem.Tag = peer;
                NodesLv.Items.Add(peer.Bruteforce.ListItem);
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            using(OpenFileDialog dialog = new OpenFileDialog())
            {
                dialog.Title = "Select the password file";
                if(dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    txtFilePath.Text = dialog.FileName;
                }
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if(txtFilePath.Text.Length == 0)
            {
                MessageBox.Show("Select a file first");
                return;
            }
            if (txtHash.Text.Length == 0)
            {
                MessageBox.Show("Fillin the hash to bruteforce");
                return;
            }
            if (NodesLv.Items.Count == 0)
            {
                MessageBox.Show("There are no nodes to use");
                return;
            }

            int LineCount = 0;
            using (StreamReader sr = new StreamReader(txtFilePath.Text))
            {
                string temp = "";
                while ((temp = sr.ReadLine()) != null)
                    LineCount++;
            }

            int LinesPerNode = LineCount / Nodes.Length;

            using (StreamReader sr = new StreamReader(txtFilePath.Text))
            {
                sr.BaseStream.Position = 0;
                for (int i = 0; i < Nodes.Length; i++ )
                {
                    Nodes[i].Bruteforce.FilePath = txtFilePath.Text;
                    Nodes[i].Bruteforce.BruteLines = LinesPerNode;

                    if(i > 0)
                    {
                        //read the lines to get to the part of where this Node should begin
                        for (int j = 0; j < LinesPerNode; j++)
                            sr.ReadLine();
                        Nodes[i].Bruteforce.BeginPosition = sr.BaseStream.Position;
                    }
                    else
                    {
                        Nodes[i].Bruteforce.BeginPosition = 0;
                    }
                }
            }

            //initialize all the nodes by telling which Algorithm to use and Target Hash
            foreach(Peer peer in Nodes)
            {
                if(!peer.Bruteforce.SetHash(txtHash.Text, Bruteforce.TYPE_MD5))
                {
                    MessageBox.Show("Invalid hash");
                    return;
                }
            }

            RunTimeSW = Stopwatch.StartNew();

            //start the bruteforce threads
            foreach(Peer peer in Nodes)
            {
                peer.Bruteforce.StartThread();
            }

            ThreadPool.QueueUserWorkItem(onUpdateNodes);
        }

        private void onUpdateNodes(object o)
        {
            long PeakSpeed = 0; 
            while(!base.IsDisposed)
            {
                long HashSpeed = 0;
                long TriedPasswords = 0;

                this.Invoke(new Invoky(() =>
                {
                    for(int i = 0; i < NodesLv.Items.Count; i++)
                    {
                        Peer peer = NodesLv.Items[i].Tag as Peer;
                        if(peer != null)
                        {
                            peer.Bruteforce.ListItem.SubItems[2].Text = peer.Bruteforce.CurrentPassword;
                            peer.Bruteforce.ListItem.SubItems[3].Text = peer.Bruteforce.TotalTries.ToString();

                            string percentage = peer.Bruteforce.TotalTries == 0 ? "0%" : Math.Round((((double)peer.Bruteforce.TotalTries / (double)peer.Bruteforce.BruteLines) * 100F), 0) + "%";
                            peer.Bruteforce.ListItem.SubItems[4].Text = peer.Bruteforce.TotalTries + " / " + peer.Bruteforce.BruteLines +  " (" + percentage + ")";

                            lock (peer.Bruteforce.ThreadSafe)
                            {
                                HashSpeed += peer.Bruteforce.Tries;
                                TriedPasswords += peer.Bruteforce.TotalTries;
                                peer.Bruteforce.Tries = 0;
                            }
                            
                            if(peer.Bruteforce.IsCracked)
                            {
                                CrackPasstxt.Text = peer.Bruteforce.CrackedPassword;
                                btnStop_Click(null, null);
                            }
                        }
                    }

                    Speedlbl.Text = "Total Hashes Per Second: " + HashSpeed;
                    TriedPasswordslbl.Text = "Tried Passwords: " + TriedPasswords;

                    if (RunTimeSW != null)
                    {
                        RunTimelbl.Text = "Run Time: " + RunTimeSW.Elapsed.Days + " Days, " + RunTimeSW.Elapsed.Hours + " Hours, " + RunTimeSW.Elapsed.Minutes + " Minutes, " + RunTimeSW.Elapsed.Seconds + " Seconds";
                    }

                    if (HashSpeed > PeakSpeed)
                    {
                        PeakSpeed = HashSpeed;
                    }
                    PeakSpeedlbl.Text = "Peak Hashes Per Second: " + PeakSpeed;

                }));

                Thread.Sleep(1000);
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            if (RunTimeSW != null)
                RunTimeSW.Stop();

            for (int i = 0; i < NodesLv.Items.Count; i++)
            {
                Peer peer = NodesLv.Items[i].Tag as Peer;
                if (peer != null)
                {
                    peer.Bruteforce.StopThread();
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace BerryCluster
{
    public partial class ImageForm : Form
    {
        public ImageForm(Image img)
        {
            InitializeComponent();
            this.pictureBox1.Image = img;
        }
    }
}

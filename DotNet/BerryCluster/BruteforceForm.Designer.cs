﻿namespace BerryCluster
{
    partial class BruteforceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFilePath = new System.Windows.Forms.TextBox();
            this.btnSelectFile = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbSha512 = new System.Windows.Forms.RadioButton();
            this.rbSha256 = new System.Windows.Forms.RadioButton();
            this.rbSha1 = new System.Windows.Forms.RadioButton();
            this.rbMd5 = new System.Windows.Forms.RadioButton();
            this.txtHash = new System.Windows.Forms.TextBox();
            this.NodesLv = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Speedlbl = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.TriedPasswordslbl = new System.Windows.Forms.Label();
            this.RunTimelbl = new System.Windows.Forms.Label();
            this.CrackPasstxt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.PeakSpeedlbl = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStart.Location = new System.Drawing.Point(475, 501);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStop.Location = new System.Drawing.Point(556, 501);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 1;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Select password file";
            // 
            // txtFilePath
            // 
            this.txtFilePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFilePath.Location = new System.Drawing.Point(126, 25);
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.ReadOnly = true;
            this.txtFilePath.Size = new System.Drawing.Size(453, 20);
            this.txtFilePath.TabIndex = 3;
            // 
            // btnSelectFile
            // 
            this.btnSelectFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectFile.Location = new System.Drawing.Point(585, 23);
            this.btnSelectFile.Name = "btnSelectFile";
            this.btnSelectFile.Size = new System.Drawing.Size(46, 23);
            this.btnSelectFile.TabIndex = 4;
            this.btnSelectFile.Text = "....";
            this.btnSelectFile.UseVisualStyleBackColor = true;
            this.btnSelectFile.Click += new System.EventHandler(this.btnSelectFile_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(88, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Hash";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbSha512);
            this.groupBox1.Controls.Add(this.rbSha256);
            this.groupBox1.Controls.Add(this.rbSha1);
            this.groupBox1.Controls.Add(this.rbMd5);
            this.groupBox1.Location = new System.Drawing.Point(22, 77);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(609, 59);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Algorithm";
            // 
            // rbSha512
            // 
            this.rbSha512.AutoSize = true;
            this.rbSha512.Location = new System.Drawing.Point(326, 27);
            this.rbSha512.Name = "rbSha512";
            this.rbSha512.Size = new System.Drawing.Size(68, 17);
            this.rbSha512.TabIndex = 3;
            this.rbSha512.Text = "SHA-512";
            this.rbSha512.UseVisualStyleBackColor = true;
            // 
            // rbSha256
            // 
            this.rbSha256.AutoSize = true;
            this.rbSha256.Location = new System.Drawing.Point(211, 27);
            this.rbSha256.Name = "rbSha256";
            this.rbSha256.Size = new System.Drawing.Size(68, 17);
            this.rbSha256.TabIndex = 2;
            this.rbSha256.Text = "SHA-256";
            this.rbSha256.UseVisualStyleBackColor = true;
            // 
            // rbSha1
            // 
            this.rbSha1.AutoSize = true;
            this.rbSha1.Location = new System.Drawing.Point(118, 27);
            this.rbSha1.Name = "rbSha1";
            this.rbSha1.Size = new System.Drawing.Size(56, 17);
            this.rbSha1.TabIndex = 1;
            this.rbSha1.Text = "SHA-1";
            this.rbSha1.UseVisualStyleBackColor = true;
            // 
            // rbMd5
            // 
            this.rbMd5.AutoSize = true;
            this.rbMd5.Checked = true;
            this.rbMd5.Location = new System.Drawing.Point(22, 27);
            this.rbMd5.Name = "rbMd5";
            this.rbMd5.Size = new System.Drawing.Size(48, 17);
            this.rbMd5.TabIndex = 0;
            this.rbMd5.TabStop = true;
            this.rbMd5.Text = "MD5";
            this.rbMd5.UseVisualStyleBackColor = true;
            // 
            // txtHash
            // 
            this.txtHash.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHash.Location = new System.Drawing.Point(126, 51);
            this.txtHash.Name = "txtHash";
            this.txtHash.Size = new System.Drawing.Size(505, 20);
            this.txtHash.TabIndex = 7;
            // 
            // NodesLv
            // 
            this.NodesLv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NodesLv.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.NodesLv.FullRowSelect = true;
            this.NodesLv.GridLines = true;
            this.NodesLv.Location = new System.Drawing.Point(22, 281);
            this.NodesLv.Name = "NodesLv";
            this.NodesLv.Size = new System.Drawing.Size(609, 213);
            this.NodesLv.TabIndex = 8;
            this.NodesLv.UseCompatibleStateImageBehavior = false;
            this.NodesLv.View = System.Windows.Forms.View.Details;
            this.NodesLv.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "IP Address";
            this.columnHeader1.Width = 110;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Hostname";
            this.columnHeader2.Width = 98;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Current Password";
            this.columnHeader3.Width = 141;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Tries";
            this.columnHeader4.Width = 81;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Progress";
            this.columnHeader5.Width = 164;
            // 
            // Speedlbl
            // 
            this.Speedlbl.AutoSize = true;
            this.Speedlbl.Location = new System.Drawing.Point(13, 22);
            this.Speedlbl.Name = "Speedlbl";
            this.Speedlbl.Size = new System.Drawing.Size(132, 13);
            this.Speedlbl.TabIndex = 9;
            this.Speedlbl.Text = "Total Hashes Per Second:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.TriedPasswordslbl);
            this.groupBox2.Controls.Add(this.RunTimelbl);
            this.groupBox2.Controls.Add(this.CrackPasstxt);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.PeakSpeedlbl);
            this.groupBox2.Controls.Add(this.Speedlbl);
            this.groupBox2.Location = new System.Drawing.Point(22, 142);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(609, 107);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            // 
            // TriedPasswordslbl
            // 
            this.TriedPasswordslbl.AutoSize = true;
            this.TriedPasswordslbl.Location = new System.Drawing.Point(438, 22);
            this.TriedPasswordslbl.Name = "TriedPasswordslbl";
            this.TriedPasswordslbl.Size = new System.Drawing.Size(88, 13);
            this.TriedPasswordslbl.TabIndex = 14;
            this.TriedPasswordslbl.Text = "Tried Passwords:";
            // 
            // RunTimelbl
            // 
            this.RunTimelbl.AutoSize = true;
            this.RunTimelbl.Location = new System.Drawing.Point(19, 46);
            this.RunTimelbl.Name = "RunTimelbl";
            this.RunTimelbl.Size = new System.Drawing.Size(59, 13);
            this.RunTimelbl.TabIndex = 13;
            this.RunTimelbl.Text = "Run Time: ";
            // 
            // CrackPasstxt
            // 
            this.CrackPasstxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CrackPasstxt.Location = new System.Drawing.Point(127, 67);
            this.CrackPasstxt.Name = "CrackPasstxt";
            this.CrackPasstxt.ReadOnly = true;
            this.CrackPasstxt.Size = new System.Drawing.Size(301, 20);
            this.CrackPasstxt.TabIndex = 12;
            this.CrackPasstxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Cracked Password: ";
            // 
            // PeakSpeedlbl
            // 
            this.PeakSpeedlbl.AutoSize = true;
            this.PeakSpeedlbl.Location = new System.Drawing.Point(237, 22);
            this.PeakSpeedlbl.Name = "PeakSpeedlbl";
            this.PeakSpeedlbl.Size = new System.Drawing.Size(133, 13);
            this.PeakSpeedlbl.TabIndex = 10;
            this.PeakSpeedlbl.Text = "Peak Hashes Per Second:";
            // 
            // BruteforceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(643, 536);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.NodesLv);
            this.Controls.Add(this.txtHash);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSelectFile);
            this.Controls.Add(this.txtFilePath);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.Name = "BruteforceForm";
            this.ShowIcon = false;
            this.Text = "Bruteforce";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtFilePath;
        private System.Windows.Forms.Button btnSelectFile;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbSha512;
        private System.Windows.Forms.RadioButton rbSha256;
        private System.Windows.Forms.RadioButton rbSha1;
        private System.Windows.Forms.RadioButton rbMd5;
        private System.Windows.Forms.TextBox txtHash;
        private System.Windows.Forms.ListView NodesLv;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Label Speedlbl;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Label PeakSpeedlbl;
        private System.Windows.Forms.TextBox CrackPasstxt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label RunTimelbl;
        private System.Windows.Forms.Label TriedPasswordslbl;
    }
}
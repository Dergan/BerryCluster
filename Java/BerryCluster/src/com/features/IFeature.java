package com.features;

import com.network.Client;
import com.utils.PayloadWriter;

public abstract class IFeature
{
	public abstract int getFeatureId();
	public abstract void onReceiveMessage(int PayloadLength) throws Exception;
	
	private Client client;
	
	public IFeature(Client client)
	{
		this.client = client;
	}
	
	public void SendMessage(PayloadWriter pw)
	{
		client.SendMessage(pw.ToByteArray());
	}
	
	protected PayloadWriter GetPayloadWriter()
	{
		PayloadWriter pw = new PayloadWriter();
		pw.WriteByte(Client.DATA);
		pw.WriteInteger(getFeatureId());
		return pw;
	}
	
	public Client GetClient()
	{
		return client;
	}
}

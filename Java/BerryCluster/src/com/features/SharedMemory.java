package com.features;

import java.io.IOException;
import java.nio.ByteBuffer;

import com.network.Client;
import com.utils.BitConverter;
import com.utils.PayloadReader;
import com.utils.PayloadWriter;

public class SharedMemory extends IFeature
{
	private final static byte RESET_MEMORY = 1;
	private final static byte ALLOCATE_MEMORY = 2;
	private final static byte WRITE_MEMORY = 3;
	private final static byte READ_MEMORY = 4;
	private final static byte READ_MEMORY_RESPONSE = 5;
	
	private byte[] Buffer;
	
	public SharedMemory(Client client)
	{
		super(client);
	
	}

	@Override
	public int getFeatureId()
	{
		return 1;
	}

	@Override
	public void onReceiveMessage(int PayloadLength) throws Exception
	{
		if(PayloadLength > 0)
		{
			Client client = super.GetClient();
			byte[] data = client.ReceiveBytes(1);
			
			if(data == null)
				return;
			
			try
			{
				switch(data[0])
				{
					case RESET_MEMORY:
					{
						Buffer = null;
						break;
					}
					case ALLOCATE_MEMORY:
					{
						data = client.ReceiveBytes(4);
						
						if(data != null)
						{
							long Size = BitConverter.toInt32(data, 0); //(bytes actually MB) back to Real MB
							Buffer = new byte[(int) Size];
						}
						break;
					}
					case WRITE_MEMORY:
					{
						data = client.ReceiveBytes(8);
						
						if(data != null)
						{
							int Pos = BitConverter.toInt32(data, 0);
							int Size = BitConverter.toInt32(data, 4);
							
							client.ReceiveTo(Size, Buffer, Pos);
							
							//cheap way...
							//int BeginPos = pr.offset;
							//for(int i = 0; i < Size; i++)
							//	Buffer[Pos + i] = Data[BeginPos + i];
						}
						break;
					}
					case READ_MEMORY:
					{
						data = client.ReceiveBytes(8);
					
						if(data != null)
						{
							int Pos = BitConverter.toInt32(data, 0);
							int Size = BitConverter.toInt32(data, 4);
							byte[] temp = new byte[Size];
							
							//cheap way
							for(int i = 0; i < Size; i++)
								temp[i] = Buffer[Pos + i];
							
							PayloadWriter pw = GetPayloadWriter();
							pw.WriteByte(READ_MEMORY_RESPONSE);
							pw.WriteBytes(temp);
							super.SendMessage(pw);
						}
						break;
					}
				}
			}
			catch (Exception e)
			{
				
			}
		}
	}
}

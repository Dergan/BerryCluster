package com.features;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Random;

import com.algorithms.CRC32Ex;
import com.network.Client;
import com.utils.BitConverter;
import com.utils.PayloadReader;
import com.utils.PayloadWriter;
import com.utils.Stopwatch;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

import javax.xml.bind.DatatypeConverter;

public class HashCollision extends IFeature
{
	//Messages
    public final static byte BRUTEFORCE = 1;
    public final static byte BRUTEFORCE_RESPONSE = 2;
    public final static byte BRUTEFORCE_SPEED = 3;
    public final static byte BRUTEFORCE_STOP = 4;
    
    //Types
    public final static byte TYPE_MD5 = 1;
    public final static byte TYPE_SHA1 = 2;
    public final static byte TYPE_SHA256 = 3;
    public final static byte TYPE_SHA512 = 4;
    public final static byte TYPE_CRC32 = 5;
    
    public byte[] TargetHash;
    public long TargetHashInt;
    
    public byte[] TargetData;
    public int UseHash = TYPE_MD5;
    public MessageDigest messageDigest;
    private Thread CollsionThread;
    private boolean ThreadIsRunning = false;
    private int StartSize = 1;
    private int HopTimeMinute = 1;
    private Stopwatch RunTimeSW;
    private byte[] BruteData;
    
    public CRC32Ex crc32;

	
	public HashCollision(Client client)
	{
		super(client);
	}
	
	@Override
	public int getFeatureId()
	{
		return 3;
	}

	@Override
	public void onReceiveMessage(int PayloadLength) throws Exception
	{
		byte[] Data = super.GetClient().ReceiveBytes(PayloadLength);
		
		if(Data.length > 0)
		{
			PayloadReader pr = new PayloadReader(Data);
			
			switch(pr.ReadByte())
			{
				case BRUTEFORCE:
				{
					TargetHash = pr.ReadBytes(pr.ReadInteger());
					TargetData = pr.ReadBytes(pr.ReadInteger());
					TargetHashInt = pr.ReadLong();
					UseHash = pr.ReadByte();
					StartSize = pr.ReadInteger();
					HopTimeMinute = pr.ReadInteger();
					
					if(UseHash == TYPE_MD5)
						messageDigest = MessageDigest.getInstance("MD5");
					else if (UseHash == TYPE_CRC32)
						crc32 = new CRC32Ex();
					
					if(messageDigest != null || crc32 != null)
					{
						this.CollsionThread = new CollisionThread(this);
						this.CollsionThread.start();
					}
					break;
				}
				case BRUTEFORCE_STOP:
				{
					ThreadIsRunning = false; //safe way of killing the thread
					break;
				}
			}
		}
	}
	
	private void SendPerformance(int SpeedPerSec, long HashesDone)
	{
		PayloadWriter pw = super.GetPayloadWriter();
		pw.WriteByte(BRUTEFORCE_SPEED);
		pw.WriteInteger(SpeedPerSec);
		pw.WriteInteger(StartSize);
		pw.WriteLong(HashesDone);
		super.SendMessage(pw);
	}
	
	private void SendCollisionData(boolean Success, byte[] CollisionData)
	{
		PayloadWriter pw = super.GetPayloadWriter();
		pw.WriteByte(BRUTEFORCE_RESPONSE);
		pw.WriteByte(Success ? (byte)1 : (byte)0);
		
		if(CollisionData == null)
		{
			pw.WriteInteger(0);
		}
		else
		{
			pw.WriteInteger(CollisionData.length);
			pw.WriteBytes(CollisionData);
		}
		super.SendMessage(pw);
	}
	
	private class CollisionThread extends Thread
	{
		private HashCollision _HashCollision;
		
        public CollisionThread(HashCollision collision)
        {
            this._HashCollision = collision;
            
        }
        
        @Override
        public void run()
        {
			boolean FoundCollision = false;
			
        	try
        	{
        		_HashCollision.RunTimeSW = new Stopwatch();
        		_HashCollision.RunTimeSW.start();
        		
        		_HashCollision.ThreadIsRunning = true;
				
				_HashCollision.BruteData = new byte[StartSize];
				Random rnd = new Random((long) Math.random());
				
				Stopwatch SpeedSW = new Stopwatch();
				SpeedSW.start();
				
				int SpeedPerSec = 0;
				long HashesDone = 0;
				
				if(_HashCollision.messageDigest != null)
				{
					_HashCollision.messageDigest.update(_HashCollision.TargetData);
				}
				
				else if(_HashCollision.crc32 != null)
				{
					_HashCollision.crc32.update(_HashCollision.TargetData, 0, _HashCollision.TargetData.length);
					_HashCollision.crc32.saveState();
				}
				
				while(_HashCollision.ThreadIsRunning && _HashCollision.GetClient().getIsConnected())
				{
					rnd.nextBytes(_HashCollision.BruteData);
					
					if(_HashCollision.messageDigest != null)
					{
						MessageDigest tempMD = (MessageDigest)_HashCollision.messageDigest.clone();
						tempMD.update(_HashCollision.BruteData);
						
						byte[] Hash = tempMD.digest();
						
						if(TargetHash.length == Hash.length)
						{
							boolean Equals = true;
							for(int y = 0; y < Hash.length; y++)
							{
								if(Hash[y] != TargetHash[y])
								{
									Equals = false;
									break;
								}
							}
							
							if(Equals)
							{
								_HashCollision.SendCollisionData(true,_HashCollision.BruteData);
								System.out.println("Found hash collision! "+ DatatypeConverter.printHexBinary(_HashCollision.BruteData));
								FoundCollision = true;
								break;
							}
						}
					}
					if(_HashCollision.crc32 != null)
					{
						_HashCollision.crc32.revertState();
						_HashCollision.crc32.update(_HashCollision.BruteData, 0, _HashCollision.BruteData.length);

						long hash = _HashCollision.crc32.getValue();
						
						if(_HashCollision.TargetHashInt == hash)
						{
							_HashCollision.SendCollisionData(true,_HashCollision.BruteData);
							System.out.println("Found hash collision! " + DatatypeConverter.printHexBinary(_HashCollision.BruteData));
							FoundCollision = true;
							break;
						}
					}
					
					
					
					SpeedPerSec++;
					HashesDone++;
					
					if(SpeedSW.getElapsedTimeSecs() >= 1)
					{
						_HashCollision.SendPerformance(SpeedPerSec, HashesDone);
						
						SpeedSW = new Stopwatch();
						SpeedSW.start();
						
						SpeedPerSec = 0;
					}
					
					if(_HashCollision.RunTimeSW.getElapsedTimeSecs() / 60 >= HopTimeMinute)
					{
						//hop to new size to try
						StartSize++;
						_HashCollision.BruteData = new byte[StartSize];
						
						_HashCollision.RunTimeSW = new Stopwatch();
		        		_HashCollision.RunTimeSW.start();
					}
				}
        	}
        	catch(Exception ex)
        	{
        		
        	}
        	
        	if(FoundCollision == true)
        	{
        		
        	}
        	
        	_HashCollision.ThreadIsRunning = false;
        }
	}
}

package com.compressions.SevenZip;

public interface ICodeProgress
{
	public void SetProgress(long inSize, long outSize);
}

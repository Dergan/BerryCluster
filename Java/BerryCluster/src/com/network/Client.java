package com.network;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;

import com.compressions.QuickLZ;
import com.features.Bruteforce;
import com.features.HashCollision;
import com.features.IFeature;
import com.features.SharedMemory;
import com.utils.BitConverter;
import com.utils.PayloadReader;
import com.utils.PayloadWriter;

public class Client
{
	public final static byte COMMAND_PONG = 0;
	public final static byte COMMAND_PING = 1;
	public final static byte COMMAND_CONNECT = 2;
	public final static byte DATA = 3;
	
	private Socket Handle;
    private InputStream inputStream;
    private OutputStream outputStream;
    
    private Object SendPacketProcessLock = new Object();
    private Thread ReceiveThread;
    
    private ArrayList<IFeature> Features;
    
    private boolean isConnected = true;
	
	public Client(Socket sock) throws IOException
	{
		this.Handle = sock;

		this.inputStream = Handle.getInputStream();
		this.outputStream = Handle.getOutputStream();
		SendClientInfo();
		
		Features = new ArrayList<IFeature>();
		Features.add(new SharedMemory(this));
		Features.add(new Bruteforce(this));
		Features.add(new HashCollision(this));
		
		ReceiveThread = new ClientReceiveThread(this);
		ReceiveThread.start();
	}
	
	//Send our device name and show the server we're connected
	private void SendClientInfo()
	{
		PayloadWriter pw = new PayloadWriter();
		pw.WriteByte(COMMAND_CONNECT);
		try
		{
			pw.WriteString(InetAddress.getLocalHost().getHostName());
		} catch (UnknownHostException e)
		{
			pw.WriteString("[Error]");
		}
		
		pw.WriteInteger(Runtime.getRuntime().availableProcessors());
		pw.WriteInteger((int)(Runtime.getRuntime().maxMemory() / 1000000));
		pw.WriteInteger((int)(Runtime.getRuntime().freeMemory() / 1000000));
		
		
		SendMessage(pw.ToByteArray());
	}
	
	public void SendMessage(byte[] data)
    {
        if(!isConnected)
            return;
        
        synchronized(SendPacketProcessLock)
        {
            try
            {
                byte[] size = BitConverter.getBytes(data.length);
                outputStream.write(size);
                outputStream.write(data);
            } catch (IOException ex) {
            	
            }
        }
    }
	
	public boolean ReceiveTo(int length, byte[] payload, int WriteOffset)
	{
		try
		{
			int received = 0;
	        while(received != length)
	        {
	            int result = inputStream.read(payload, WriteOffset + received, length-received);
	            
	            if(result == -1)
	            {
	            	isConnected = false;
	                return false;
	            }
	            received += result;
	        }
	        return true;
		}
		catch(Exception ex)
		{
			return false;
		}
	}
	
	public byte[] ReceiveBytes(int length) throws IOException
    {
        int received = 0;
        byte[] ret = new byte[length];
        while(received != length)
        {
            int result = inputStream.read(ret, received, length-received);
            
            if(result == -1)
            {
            	isConnected = false;
                return new byte[0];
            }
            received += result;
        }
        return ret;
    }
    
    private byte[] ReceivePacket()
    {
        try
        {
            byte[] size_data = ReceiveBytes(4);
            if(size_data.length == 0)
                return new byte[0];
            int size = BitConverter.toInt32(size_data, 0);
            byte[] payload = ReceiveBytes(size);
            
            return payload;
        } catch (Exception ex) {
            return new byte[0];
        }
    }
    
    private int ReceivePacketSize()
    {
        try
        {
            byte[] size_data = ReceiveBytes(4);
            if(size_data.length == 0)
                return 0;
            return BitConverter.toInt32(size_data, 0);
        } catch (Exception ex) {
            return 0;
        }
    }
    
    public boolean getIsConnected()
    {
    	return isConnected;
    }
    
    public void Disconnect()
    {
        try
        {
        	isConnected = false;
            Handle.close();
        } catch (IOException ex) { }
    }
    
    static class MyOutputStream extends java.io.OutputStream
	{
		byte[] _buffer;
		int _size;
		int _pos;
		
		public MyOutputStream(byte[] buffer)
		{
			_buffer = buffer;
			_size = _buffer.length;
		}
		
		public void reset()
		{ 
			_pos = 0; 
		}
		
		public void write(int b) throws IOException
		{
			if (_pos >= _size)
				throw new IOException("Error");
			_buffer[_pos++] = (byte)b;
		}
		
		public int size()
		{
			return _pos;
		}
	};
    
    static class MyInputStream extends java.io.InputStream
	{
		byte[] _buffer;
		int _size;
		int _pos;
		
		public MyInputStream(byte[] buffer, int size)
		{
			_buffer = buffer;
			_size = size;
		}
		
		public void reset()
		{ 
			_pos = 0; 
		}
		
		public int read()
		{
			if (_pos >= _size)
				return -1;
			return _buffer[_pos++] & 0xFF;
		}
	};
	
	private class ClientReceiveThread extends Thread
	{
		private Client client;
        public ClientReceiveThread(Client client)
        {
            this.client = client;
        }
        
        @Override
        public void run()
        {
        	try
            {
                while(client.Handle.isConnected())
                {
                    int PayloadSize = client.ReceivePacketSize();
                    if(PayloadSize == 0)
                    {
                        client.Disconnect();
                        break;
                    }
                    
                    byte[] DataHeader = new byte[5];
                    if(!client.ReceiveTo(DataHeader.length, DataHeader, 0))
                    	break;
                    
                    PayloadReader pr = new PayloadReader(DataHeader);
                    String lastError = "";
                    
                	switch(pr.ReadByte())
                	{
                    	case DATA:
                    	{
                    		int FeatureId = pr.ReadInteger();
                    		
                    		for(int i = 0; i < Features.size(); i++)
                    		{
                    			IFeature feature = Features.get(i);
                    			if(feature.getFeatureId() == FeatureId)
                    			{
                    				try
                    				{
                    					feature.onReceiveMessage(PayloadSize - DataHeader.length);
                    				}
                    				catch(Exception ex)
                    				{
                    					lastError = ex.getMessage();
                    					System.out.println(ex.getMessage());
                    				}
                    			}
                    		}
                    		
                    		/*
                    		//QuickLZ
                    		byte[] Compressed = QuickLZ.compress(TempData, 3);
                    		PayloadWriter pw = new PayloadWriter();
                    		pw.WriteByte((byte)END_TASK);
                    		pw.WriteInteger(TempData.length);
                    		pw.WriteInteger(Compressed.length);
                    		client.SendMessage(pw.ToByteArray());
                    		
                    		//compress with LZMA
                    		com.compressions.SevenZip.Compression.LZMA.Encoder encoder = new com.compressions.SevenZip.Compression.LZMA.Encoder();
                    		
                    		if (!encoder.SetDictionarySize(512000))
                    			throw new Exception("Incorrect dictionary size");
                    		
                    		ByteArrayOutputStream propStream = new ByteArrayOutputStream();
                    		encoder.WriteCoderProperties(propStream);
                    		byte[] propArray = propStream.toByteArray();
                    		
                    		MyInputStream inStream = new MyInputStream(TempData, TempData.length);
                    		
                    		byte[] compressedBuffer = new byte[TempData.length + 10000];
                    		MyOutputStream compressedStream = new MyOutputStream(compressedBuffer);
                    		
                    		long sizeIn = -1;
                    		long sizeOut = -1;
                    		
                    		encoder.Code(inStream, compressedStream, sizeIn, sizeOut, null);
                    		
                    		PayloadWriter pw = new PayloadWriter();
                    		pw.WriteByte((byte)END_TASK);
                    		pw.WriteInteger(TempData.length);
                    		pw.WriteInteger(0);
                    		client.SendMessage(pw.ToByteArray());*/
                    		
                    		break;
                    	}
                	}
                    
                    pr.Dispose();
                }
            } catch (Exception ex) {
            	
            }
        }
	}
}

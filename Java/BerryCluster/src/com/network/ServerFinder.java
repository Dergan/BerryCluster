package com.network;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.Enumeration;

public class ServerFinder
{
	public static Socket FindServer()
	{
		try
		{
			Enumeration<NetworkInterface> n = NetworkInterface.getNetworkInterfaces();
		    for (; n.hasMoreElements();)
		    {
		        NetworkInterface e = n.nextElement();
	
		        Enumeration<InetAddress> a = e.getInetAddresses();
		        for (; a.hasMoreElements();)
		        {
		            InetAddress addr = a.nextElement();
		            
		            if(addr.getHostAddress().startsWith(com.main.Main.LocalSubnet))
		            {
		            	String LanIP = addr.getHostAddress();
		            	String StartIP = LanIP.substring(0, LanIP.lastIndexOf(".") + 1);
		    		    
		    		    for(int i = 2; i < 250; i++) //should be 250 on release 
		    		    {
		    		    	try
		    		    	{
		    		    		Socket client = new Socket();
		    		    		client.connect(new InetSocketAddress(StartIP + i, 8322), 100);
		    		    		if(client.isConnected())
		    		    		{
		    		    			byte[] TempPacket = new byte[6];
		    		    			TempPacket[0] = 2; //header
		    		    			TempPacket[1] = 0;
		    		    			TempPacket[2] = 0;
		    		    			TempPacket[3] = 0;
		    		    			TempPacket[4] = 1; //payload
		    		    			TempPacket[5] = 5;
		    		    			
		    		    			client.getOutputStream().write(TempPacket);
		    		    			
		    		    			int read = client.getInputStream().read(TempPacket);
		    		    			if(read == 6)
		    		    			{
		    		    				if(TempPacket[5] == 6)
		    		    				{
		    		    					return client;
		    		    				}
		    		    			}
		    		    		}
		    		    	}
		    		    	catch(Exception ex) { }
		    		    }
		            }
		        }
		    }
			return null;
		}
		catch(Exception ex)
		{
			return null;
		}
	}
}

package com.main;

import java.net.Socket;
import com.network.Client;
import com.network.ServerFinder;

public class Main
{
	//just some main settings
	public final static String LocalSubnet = "192.168.";
	
	public static void main(String[] args) throws Exception
	{
		/*byte[] bytes = "derp".getBytes();
		Stopwatch sw = new Stopwatch();
		sw.start();
		int Speed = 0;
		MessageDigest m = MessageDigest.getInstance("MD5");
		
		while(true)
		{
			m.reset();
			m.update(bytes);
			Speed++;
			
			if(sw.getElapsedTimeSecs() >= 1)
			{
				System.out.println("Speed: " + Speed);
				Speed = 0;
				sw = new Stopwatch();
				sw.start();
			}
		}*/
		
		System.out.println("Connecting to master server...");
		
		while(true)
		{
			Socket socket = ServerFinder.FindServer();
			
			if(socket != null)
			{
				System.out.println("Found server");
				Client client = new Client(socket);
				
				while(client.getIsConnected())
					Thread.sleep(1000); //keep program running
				
				System.out.println("Disconnected");
			}
			else
			{
				System.out.println("Could not find the server");
			}
			Thread.sleep(1000);
		}
		
		//LzmaBench.LzmaBenchmark(30, 512000);
		
		/*Random rnd = new Random();
		QuickLZ derp = new QuickLZ();
		int Speed = 0;
		byte[] RandomData = new byte[50000];

		Stopwatch sw = new Stopwatch();
		sw.start();
		
		while(true)
		{
			derp.compress(RandomData, 3);
			Speed++;
			
			if(sw.getElapsedTimeSecs() >= 1)
			{
				System.out.println("Speed: " + Speed + ", " + (int)(((Speed * RandomData.length) / 1000) / 1000) + "MBps");
				Speed = 0;
				sw = new Stopwatch();
				sw.start();
			}
		}*/
	}

}
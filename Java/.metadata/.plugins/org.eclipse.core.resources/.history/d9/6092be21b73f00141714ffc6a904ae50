package com.network;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import com.compressions.QuickLZ;
import com.compressions.SevenZip.LzmaBench.MyOutputStream;
import com.utils.BitConverter;
import com.utils.PayloadReader;
import com.utils.PayloadWriter;

public class Client
{
	private final byte COMMAND_PONG = 0;
	private final byte COMMAND_PING = 1;
	private final byte COMMAND_CONNECT = 2;
	private final byte START_TASK = 3;
	private final byte END_TASK = 4;
	
	private Socket Handle;
    private InputStream inputStream;
    private OutputStream outputStream;
    
    private Object SendPacketProcessLock = new Object();
    private Thread ReceiveThread;
	
	public Client(Socket sock) throws IOException
	{
		this.Handle = sock;

		this.inputStream = Handle.getInputStream();
		this.outputStream = Handle.getOutputStream();
		SendConnect();
		
		ReceiveThread = new ClientReceiveThread(this);
		ReceiveThread.start();
	}
	
	//Send our device name and show the server we're connected
	private void SendConnect()
	{
		PayloadWriter pw = new PayloadWriter();
		pw.WriteByte(COMMAND_CONNECT);
		try {
			pw.WriteString(InetAddress.getLocalHost().getHostName());
		} catch (UnknownHostException e) {
			pw.WriteString("[Error]");
		}
		
		pw.WriteInteger(Runtime.getRuntime().availableProcessors());
		
		SendMessage(pw.ToByteArray());
	}
	
	public void SendMessage(byte[] data)
    {
        if(!Handle.isConnected())
            return;
        
        synchronized(SendPacketProcessLock)
        {
            try
            {
                byte[] size = BitConverter.getBytes((short)data.length);
                outputStream.write(size);
                outputStream.write(data);
            } catch (IOException ex) {
            	
            }
        }
    }
	
	private byte[] ReceiveBytes(int length) throws IOException
    {
        int received = 0;
        byte[] ret = new byte[length];
        while(received != length)
        {
            int result = inputStream.read(ret, received, length-received);
            
            if(result == -1)
                return new byte[0];
            received += result;
        }
        return ret;
    }
    
    private byte[] ReceivePacket(boolean decryptTraffic)
    {
        try
        {
            byte[] size_data = ReceiveBytes(2);
            if(size_data.length == 0)
                return new byte[0];
            int size = BitConverter.toInt16(size_data, 0);
            byte[] payload = ReceiveBytes(size);
            
            return payload;
        } catch (Exception ex) {
            return new byte[0];
        }
    }
    
    public void Disconnect()
    {
        try
        {
            Handle.close();
        } catch (IOException ex) { }
    }
    
    static class MyOutputStream extends java.io.OutputStream
	{
		byte[] _buffer;
		int _size;
		int _pos;
		
		public MyOutputStream(byte[] buffer)
		{
			_buffer = buffer;
			_size = _buffer.length;
		}
		
		public void reset()
		{ 
			_pos = 0; 
		}
		
		public void write(int b) throws IOException
		{
			if (_pos >= _size)
				throw new IOException("Error");
			_buffer[_pos++] = (byte)b;
		}
		
		public int size()
		{
			return _pos;
		}
	};
    
    static class MyInputStream extends java.io.InputStream
	{
		byte[] _buffer;
		int _size;
		int _pos;
		
		public MyInputStream(byte[] buffer, int size)
		{
			_buffer = buffer;
			_size = size;
		}
		
		public void reset()
		{ 
			_pos = 0; 
		}
		
		public int read()
		{
			if (_pos >= _size)
				return -1;
			return _buffer[_pos++] & 0xFF;
		}
	};
	
	private class ClientReceiveThread extends Thread
	{
		private Client client;
        public ClientReceiveThread(Client client)
        {
            this.client = client;
        }
        
        @Override
        public void run()
        {
        	try
            {
                while(client.Handle.isConnected())
                {
                    byte[] data = client.ReceivePacket(true);
                    if(data.length == 0)
                    {
                        client.Disconnect();
                        break;
                    }
                    
                    PayloadReader pr = new PayloadReader(data);
                    //System.out.println("Received Size:" + data.length);

                    if(data.length > 0)
                    {
                    	switch(pr.ReadByte())
                    	{
	                    	case START_TASK:
	                    	{
	                    		byte[] TempData = pr.ReadBytes(data.length - 1);
	                    		byte[] Compressed = null;//QuickLZ.compress(TempData, 3);
	                    		
	                    		
	                    		
	                    		
	                    		//compress with LZMA
	                    		com.compressions.SevenZip.Compression.LZMA.Encoder encoder = new com.compressions.SevenZip.Compression.LZMA.Encoder();
	                    		
	                    		if (!encoder.SetDictionarySize(512000))
	                    			throw new Exception("Incorrect dictionary size");
	                    		
	                    		ByteArrayOutputStream propStream = new ByteArrayOutputStream();
	                    		encoder.WriteCoderProperties(propStream);
	                    		byte[] propArray = propStream.toByteArray();
	                    		
	                    		MyInputStream inStream = new MyInputStream(TempData, TempData.length);
	                    		
	                    		byte[] compressedBuffer = new byte[TempData.length + 10000];
	                    		MyOutputStream compressedStream = new MyOutputStream(compressedBuffer);
	                    		
	                    		encoder.Code(inStream, compressedStream, -1, -1, null);
	                    		
	                    		
	                    		PayloadWriter pw = new PayloadWriter();
	                    		pw.WriteByte((byte)END_TASK);
	                    		pw.WriteInteger(TempData.length);
	                    		pw.WriteInteger(Compressed.length);
	                    		
	                    		client.SendMessage(pw.ToByteArray());
	                    		break;
	                    	}
                    	}
                    }
                    
                    pr.Dispose();
                }
            } catch (Exception ex) {
            	
            }
        }
	}
}
